<?php

namespace App\Http\Controllers;

use App\Models\Tarik;
use Illuminate\Http\Request;

class TarikController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Tarik  $tarik
     * @return \Illuminate\Http\Response
     */
    public function show(Tarik $tarik)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Tarik  $tarik
     * @return \Illuminate\Http\Response
     */
    public function edit(Tarik $tarik)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Tarik  $tarik
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tarik $tarik)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Tarik  $tarik
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tarik $tarik)
    {
        //
    }
}
