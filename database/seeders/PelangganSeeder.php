<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PelangganSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        $arr_pelanggan = [
        	[
        		'nama' => 'Deny Caknan',
				'jenis_kelamin' => 'L',
				'kota' => 'Sleman',
				'kecamatan' => 'Depok',
				'kelurahan' => 'Condongcatur',
				'alamat_jalan' => 'Ringroad',
				'email' => 'deny@gmail.com',
				'password' => sha1('qwerty'),
				'nomor_rek' => '0987877899',
				'jenis' => 'Prioritas',
				'saldo' => 0
        	],
        	[
        		'nama' => 'Sujiwo Tedjo',
				'jenis_kelamin' => 'L',
				'kota' => 'Yogyakarta (Kota)',
				'kecamatan' => 'Danurejan',
				'kelurahan' => 'Suryatmajan',
				'alamat_jalan' => 'Dalam Kota',
				'email' => 'tedjo@gmail.com',
				'password' => sha1('qwerty'),
				'nomor_rek' => '0987874842',
				'jenis' => 'Prioritas',
				'saldo' => 0
        	],
        	[
        		'nama' => 'Suciyati',
				'jenis_kelamin' => 'P',
				'kota' => 'Yogyakarta (Kota)',
				'kecamatan' => 'Danurejan',
				'kelurahan' => 'Suryatmajan',
				'alamat_jalan' => 'Pingir Benteng',
				'email' => 'suci_caem@gmail.com',
				'password' => sha1('qwerty'),
				'nomor_rek' => '0987856743',
				'jenis' => 'Biasa',
				'saldo' => 0
        	],

        ];

        foreach ($arr_pelanggan as $key => $value) {
        	$id_pelanggan = DB::table('pelanggan')->insertGetId([
	            'nama' => $value['nama'],
				'jenis_kelamin' => $value['jenis_kelamin'],
				'kota' => $value['kota'],
				'kecamatan' => $value['kecamatan'],
				'kelurahan' => $value['kelurahan'],
				'alamat_jalan' => $value['alamat_jalan'],
				'email' => $value['email'],
				'password' => $value['password'],
	        ]);

        	DB::table('tabungan')->insert([
        		'id_pelanggan' => $id_pelanggan,
        		'nomor_rek' => $value['nomor_rek'],
        		'jenis' => $value['jenis'],
        		'saldo' => $value['saldo'],
        	]);
        }
    }
}
