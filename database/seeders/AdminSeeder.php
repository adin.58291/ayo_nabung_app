<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('admin')->insert([
            'nama' => 'Admin Super',
            'username' => 'admin',
            'password' => sha1('qwerty'),
        ]);

        DB::table('admin')->insert([
            'nama' => 'Bawahannya Admin',
            'username' => 'admin2',
            'password' => sha1('12345'),
        ]);
    }
}
