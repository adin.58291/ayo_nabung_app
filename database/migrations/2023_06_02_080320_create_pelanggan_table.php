<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pelanggan', function (Blueprint $table) {
           $table->increments('id_pelanggan');
           $table->string('nama', 100);
           $table->string('jenis_kelamin', 1);          
           $table->string('kota', 150);
           $table->string('kecamatan', 150);
           $table->string('kelurahan', 150);           
           $table->string('alamat_jalan', 150); 
           $table->string('email', 100);
           $table->string('password', 100);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pelanggan');
    }
};
